import 'dart:convert';

import 'package:partida/partida.dart';

class PuntuacionJugador {
  final Jugador jugador;
  final int porAzules;
  final int porVerdes;
  final int porRosas;
  final int porNegras;
  
  PuntuacionJugador({
    required this.jugador,
    required this.porAzules,
    required this.porVerdes,
    required this.porRosas,
    required this.porNegras,
  });

  int get total => porAzules + porNegras + porVerdes + porRosas;

  PuntuacionJugador copyWith({
    Jugador? jugador,
    int? porAzules,
    int? porVerdes,
    int? porRosas,
    int? porNegras,
  }) {
    return PuntuacionJugador(
      jugador: jugador ?? this.jugador,
      porAzules: porAzules ?? this.porAzules,
      porVerdes: porVerdes ?? this.porVerdes,
      porRosas: porRosas ?? this.porRosas,
      porNegras: porNegras ?? this.porNegras,
    );
  }

  Map<String, dynamic> toMap() {
    return {
      'jugador': jugador.toMap(),
      'porAzules': porAzules,
      'porVerdes': porVerdes,
      'porRosas': porRosas,
      'porNegras': porNegras,
    };
  }

  factory PuntuacionJugador.fromMap(Map<String, dynamic> map) {
    return PuntuacionJugador(
      jugador: Jugador.fromMap(map['jugador']),
      porAzules: map['porAzules']?.toInt() ?? 0,
      porVerdes: map['porVerdes']?.toInt() ?? 0,
      porRosas: map['porRosas']?.toInt() ?? 0,
      porNegras: map['porNegras']?.toInt() ?? 0,
    );
  }

  String toJson() => json.encode(toMap());

  factory PuntuacionJugador.fromJson(String source) => PuntuacionJugador.fromMap(json.decode(source));

  @override
  String toString() {
    return 'PuntuacionJugador(jugador: $jugador, porAzules: $porAzules, porVerdes: $porVerdes, porRosas: $porRosas, porNegras: $porNegras)';
  }

  @override
  bool operator ==(Object other) {
    if (identical(this, other)) return true;
  
    return other is PuntuacionJugador &&
      other.jugador == jugador &&
      other.porAzules == porAzules &&
      other.porVerdes == porVerdes &&
      other.porRosas == porRosas &&
      other.porNegras == porNegras;
  }

  @override
  int get hashCode {
    return jugador.hashCode ^
      porAzules.hashCode ^
      porVerdes.hashCode ^
      porRosas.hashCode ^
      porNegras.hashCode;
  }
}
