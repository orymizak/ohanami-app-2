import 'dart:convert';
import 'package:partida/src/problemas.dart';
import 'jugador.dart';

const int ninguna = 0;
const int maxCartasPuntuacionRonda1 = 10;
const int maxCartasPuntuacionRonda2 = 20;
const int maxCartasPuntuacionRonda3 = 30;

class CartasRonda1 {
  final Jugador jugador;
  final int cuantasAzules;

  CartasRonda1({required this.jugador, required this.cuantasAzules}) {
    if (cuantasAzules < ninguna) throw ProblemaAzulesNegativas();
    if (cuantasAzules > maxCartasPuntuacionRonda1) throw ProblemaDemasiadasAzules();
  }


  CartasRonda1 copyWith({
    Jugador? jugador,
    int? cuantasAzules,
  }) {
    return CartasRonda1(
      jugador: jugador ?? this.jugador,
      cuantasAzules: cuantasAzules ?? this.cuantasAzules,
    );
  }

  Map<String, dynamic> toMap() {
    return {
      'jugador': jugador.toMap(),
      'cuantasAzules': cuantasAzules,
    };
  }

  factory CartasRonda1.fromMap(Map<String, dynamic> map) {
    return CartasRonda1(
      jugador: Jugador.fromMap(map['jugador']),
      cuantasAzules: map['cuantasAzules']?.toInt() ?? 0,
    );
  }

  String toJson() => json.encode(toMap());

  factory CartasRonda1.fromJson(String source) => CartasRonda1.fromMap(json.decode(source));

  @override
  String toString() => 'PRonda1(jugador: $jugador, cuantasAzules: $cuantasAzules)';

  @override
  bool operator ==(Object other) {
    if (identical(this, other)) return true;
  
    return other is CartasRonda1 &&
      other.jugador == jugador &&
      other.cuantasAzules == cuantasAzules;
  }

  @override
  int get hashCode => jugador.hashCode ^ cuantasAzules.hashCode;
}

class CartasRonda2 {
  final Jugador jugador;
  final int cuantasAzules;
  final int cuantasVerdes;

  CartasRonda2({required this.jugador, required this.cuantasAzules, required this.cuantasVerdes}) {
    if (cuantasAzules < ninguna) throw ProblemaAzulesNegativas();
    if (cuantasVerdes < ninguna) throw ProblemaVerdesNegativas();
  }


  CartasRonda2 copyWith({
    Jugador? jugador,
    int? cuantasAzules,
    int? cuantasVerdes,
  }) {
    return CartasRonda2(
      jugador: jugador ?? this.jugador,
      cuantasAzules: cuantasAzules ?? this.cuantasAzules,
      cuantasVerdes: cuantasVerdes ?? this.cuantasVerdes,
    );
  }

  Map<String, dynamic> toMap() {
    return {
      'jugador': jugador.toMap(),
      'cuantasAzules': cuantasAzules,
      'cuantasVerdes': cuantasVerdes,
    };
  }

  factory CartasRonda2.fromMap(Map<String, dynamic> map) {
    return CartasRonda2(
      jugador: Jugador.fromMap(map['jugador']),
      cuantasAzules: map['cuantasAzules']?.toInt() ?? 0,
      cuantasVerdes: map['cuantasVerdes']?.toInt() ?? 0,
    );
  }

  String toJson() => json.encode(toMap());

  factory CartasRonda2.fromJson(String source) => CartasRonda2.fromMap(json.decode(source));

  @override
  String toString() => 'PRonda2(jugador: $jugador, cuantasAzules: $cuantasAzules, cuantasVerdes: $cuantasVerdes)';

  @override
  bool operator ==(Object other) {
    if (identical(this, other)) return true;
  
    return other is CartasRonda2 &&
      other.jugador == jugador &&
      other.cuantasAzules == cuantasAzules &&
      other.cuantasVerdes == cuantasVerdes;
  }

  @override
  int get hashCode => jugador.hashCode ^ cuantasAzules.hashCode ^ cuantasVerdes.hashCode;
}

class CartasRonda3 {
  final int cuantasVerdes;
  final int cuantasAzules;
  final int cuantasNegras;
  final int cuantasRosas;
  final Jugador jugador;
  CartasRonda3(
      {required this.jugador,
      required this.cuantasVerdes,
      required this.cuantasAzules,
      required this.cuantasNegras,
      required this.cuantasRosas}) {
    if (cuantasAzules < ninguna) throw ProblemaAzulesNegativas();
    if (cuantasVerdes < ninguna) throw ProblemaVerdesNegativas();
    if (cuantasNegras < ninguna) throw ProblemaNegrasNegativas();
    if (cuantasRosas < ninguna) throw ProblemaRosasNegativas();
  }



  CartasRonda3 copyWith({
    int? cuantasVerdes,
    int? cuantasAzules,
    int? cuantasNegras,
    int? cuantasRosas,
    Jugador? jugador,
  }) {
    return CartasRonda3(
      cuantasVerdes: cuantasVerdes ?? this.cuantasVerdes,
      cuantasAzules: cuantasAzules ?? this.cuantasAzules,
      cuantasNegras: cuantasNegras ?? this.cuantasNegras,
      cuantasRosas: cuantasRosas ?? this.cuantasRosas,
      jugador: jugador ?? this.jugador,
    );
  }

  Map<String, dynamic> toMap() {
    return {
      'cuantasVerdes': cuantasVerdes,
      'cuantasAzules': cuantasAzules,
      'cuantasNegras': cuantasNegras,
      'cuantasRosas': cuantasRosas,
      'jugador': jugador.toMap(),
    };
  }

  factory CartasRonda3.fromMap(Map<String, dynamic> map) {
    return CartasRonda3(
      cuantasVerdes: map['cuantasVerdes']?.toInt() ?? 0,
      cuantasAzules: map['cuantasAzules']?.toInt() ?? 0,
      cuantasNegras: map['cuantasNegras']?.toInt() ?? 0,
      cuantasRosas: map['cuantasRosas']?.toInt() ?? 0,
      jugador: Jugador.fromMap(map['jugador']),
    );
  }

  String toJson() => json.encode(toMap());

  factory CartasRonda3.fromJson(String source) => CartasRonda3.fromMap(json.decode(source));

  @override
  String toString() {
    return 'PRonda3(cuantasVerdes: $cuantasVerdes, cuantasAzules: $cuantasAzules, cuantasNegras: $cuantasNegras, cuantasRosas: $cuantasRosas, jugador: $jugador)';
  }

  @override
  bool operator ==(Object other) {
    if (identical(this, other)) return true;
  
    return other is CartasRonda3 &&
      other.cuantasVerdes == cuantasVerdes &&
      other.cuantasAzules == cuantasAzules &&
      other.cuantasNegras == cuantasNegras &&
      other.cuantasRosas == cuantasRosas &&
      other.jugador == jugador;
  }

  @override
  int get hashCode {
    return cuantasVerdes.hashCode ^
      cuantasAzules.hashCode ^
      cuantasNegras.hashCode ^
      cuantasRosas.hashCode ^
      jugador.hashCode;
  }
}
