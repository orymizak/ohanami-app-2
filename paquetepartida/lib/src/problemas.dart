
 
  // problema jugadores

class ProblemaJugadoresRepetidos extends Error implements Exception {
  @override
  String toString() => 'Atención: no puede haber jugadores con el mismo nombre.';
}

class ProblemaJugadorNombreVacio extends Error implements Exception {
  @override
  String toString() => 'Error: todos los jugadores deben tener nombre.';
}

class ProblemaNumeroJugadoresMinimo extends Error implements Exception {
  @override
  String toString() => 'Error: se necesita al menos 2 jugadores para continuar.';
}

class ProblemaNumeroJugadoresMaximo extends Error implements Exception {
  @override
  String toString() => 'Error: se ha sobrepasado el limite de jugadores (máx. 4).';
}

class ProblemaJugadoresInconsistentes extends Error implements Exception {
  @override
  String toString() => 'Error: Los jugadores a puntuar no coinciden.';
}

class ProblemaOrdenIncorrecto extends Error implements Exception {
  @override
  String toString() => 'Error: Los jugadores a puntuar no coinciden.';
}

  // problema cartas negativas

class ProblemaAzulesNegativas extends Error implements Exception {
  @override
  String toString() => 'Atención: las cartas azules no pueden ser negativas.';
}

class ProblemaVerdesNegativas extends Error implements Exception {
  @override
  String toString() => 'Atención: las cartas azules no pueden ser negativas.';
}

class ProblemaRosasNegativas extends Error implements Exception {
  @override
  String toString() => 'Atención: las cartas rosas no pueden ser negativas.';
}

class ProblemaNegrasNegativas extends Error implements Exception {
  @override
  String toString() => 'Atención: las cartas negras no pueden ser negativas.';
}


  // problemas cartas excedente

class ProblemaDemasiadasAzules extends Error implements Exception {
  @override
  String toString() => 'Atención: las cartas azules de algún jugador superan el límite de 10 por ronda.';
}

class ProblemaDemasiadasVerdes extends Error implements Exception {
  @override
  String toString() => 'Atención: las cartas verdes de algún jugador superan el límite de 10 por ronda.';
}

class ProblemaDemasiadasRosas extends Error implements Exception {
  @override
  String toString() => 'Atención: las cartas rosas de algún jugador superan el límite de 10 por ronda.';
}

class ProblemaDemasiadasNegras extends Error implements Exception {
  @override
  String toString() => 'Atención: las cartas negras de algún jugador superan el límite de 10 por ronda.';
}

class ProblemaExcesoCartas extends Error implements Exception {
  @override
  String toString() => 'Atención: el total de cartas de algún jugador excede el limite de 10 por ronda.';
}

  // problemas cartas menor que ronda anterior

class ProblemaDisminucionAzules extends Error implements Exception {
  @override
  String toString() => 'Atención: las cartas azules de algún jugador son menores en esta ronda.';
}

class ProblemaDisminucionVerdes extends Error implements Exception {
  @override
  String toString() => 'Atención: las cartas verdes de algún jugador son menores en esta ronda.';
}
