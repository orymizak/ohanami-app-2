import 'dart:convert';
import 'package:flutter_sesion/flutter_session.dart';
import 'package:mongo_dart/mongo_dart.dart';
import 'package:partida/partida.dart';
import 'constantes.dart';
import 'usuario.dart';

class RepositorioMongo {
  late Db conn;
  RepositorioMongo();

  // flutter check
  Future<bool> checarRed() async {
    conn = await Db.create(URL);
    try {
      await conn.open();
      return await conn.close().then((value) => true);
    } catch (e) {
      return false;
    }
  }

  // flutter check
  Future<Usuario> registrarPartida({required Partida partida}) async {
    Usuario usuario = Usuario.fromJson(await FlutterSession().get("datos"));
    usuario.partidas.add(partida);
    conn = await Db.create(URL);
    await conn.open();
    return await conn
        .collection('usuarios')
        .update(
            await conn.collection('usuarios').findOne(
                where.eq('usuario', await FlutterSession().get("usuario"))),
            Push({'partidas': jsonDecode(partida.toJson())}).build())
        .then((value) async {
      await FlutterSession().set("datos", usuario);
      return usuario;
    });
  }

  // flutter check
  Future<bool> eliminarPartida({required int indice}) async {
    Usuario usuario = Usuario.fromJson(await FlutterSession().get("datos"));
    usuario.partidas.removeAt(indice);
    var partidasJson = jsonDecode(
        json.encode(usuario.partidas.map((x) => x.toMap()).toList()));
    conn = await Db.create(URL);
    await conn.open();
    return await conn
        .collection('usuarios')
        .updateOne(where.eq('usuario', usuario.usuario.toString()),
            modify.set('partidas', partidasJson))
        .then((value) async {
      await FlutterSession().set("datos", jsonEncode(usuario.toJson()));
      return true;
    });
  }

  // flutter check
  Future<bool> registradoUsuario({required Usuario usuarioRemoto}) async {
    conn = await Db.create(URL);
    await conn.open();
    return await conn
        .collection('usuarios')
        .find(where.eq('usuario', usuarioRemoto.usuario.toString()))
        .toList()
        .then((value) => value.isNotEmpty ? true : false);
  }

  // flutter check
  Future<bool> registrarUsuario({required Usuario usuarioRemoto}) async {
    conn = await Db.create(URL);
    await conn.open();
    await FlutterSession().set("usuario", usuarioRemoto.usuario);
    await FlutterSession().set("datos", usuarioRemoto);
    await FlutterSession().set("tipoConexion", 1);
    return await conn
        .collection('usuarios')
        .insert(jsonDecode(usuarioRemoto.toJson()))
        .then((value) => true);
  }

  // flutter check
  Future<bool> verificarSesion({required Usuario usuarioRemoto}) async {
    conn = await Db.create(URL);
    await conn.open();
    return await conn
        .collection('usuarios')
        .findOne(where
            .eq('usuario', usuarioRemoto.usuario.toString())
            .and(where.eq('password', usuarioRemoto.password.toString())))
        .then((value) => value == null ? false : true);
  }

  // flutter check
  Future<Usuario> recuperarUsuario(usuario) async {
    conn = await Db.create(URL);
    await conn.open();
    var colexion = conn.collection('usuarios');
    var datos = await colexion.findOne(where.eq('usuario', usuario));
    await conn.close();
    datos!.remove('_id');
    Usuario us = Usuario.fromMap(datos);
    await FlutterSession().set("usuario", us.usuario);
    await FlutterSession().set("tipoConexion", 1);
    await FlutterSession().set("datos", us);
    return us;
  }
}
