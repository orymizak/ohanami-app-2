import 'package:ohanami/vistas/vista_inicial_bloc.dart';
import 'package:flutter/material.dart';

class Aplicacion extends StatelessWidget {
  const Aplicacion({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      theme: ThemeData(

      ),
      debugShowCheckedModeBanner: false,
      home: const Scaffold(
        body: VistaInicial(),
      ),
    );
  }
}
