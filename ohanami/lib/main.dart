import 'package:ohanami/aplicacion.dart';
import 'package:ohanami/bloc_ohanami/bloc_ohanami.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';

void main() {
  runApp(const MyApp());
}
        
class MyApp extends StatelessWidget {
  const MyApp({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return BlocProvider(
      create: (_) => OhanamiBloc(),
      child: const Aplicacion(),
    );
  }
}
