import 'package:db_paquete/db_paquete.dart';
import 'package:ohanami/db/db_local.dart';
import 'package:flutter/cupertino.dart';
import 'dart:ui' as ui;
import 'package:flutter/material.dart';

var logoDir = "lib/src/logo.png";

Widgets widget = Widgets();
RepositorioLocal usuarioLocal = RepositorioLocal();
RepositorioMongo usuarioRemoto = RepositorioMongo();

double ancho = ui.window.physicalSize.width / ui.window.devicePixelRatio;
double alto = ui.window.physicalSize.height / ui.window.devicePixelRatio;

showAlertDialog3(BuildContext context) {
  showDialog(
      barrierDismissible: false,
      context: context,
      builder: (BuildContext context) {
        return AlertDialog(
          content: Row(
            children: [
              const CircularProgressIndicator(),
              Container(
                margin: const EdgeInsets.only(left: 35),
                child: const Text("Abandonando partida..."),
              ),
            ],
          ),
        );
      });
}

showAlertDialog2(BuildContext context) {
  showDialog(
      barrierDismissible: false,
      context: context,
      builder: (BuildContext context) {
        return AlertDialog(
          content: Row(
            children: [
              const CircularProgressIndicator(),
              Container(
                margin: const EdgeInsets.only(left: 35),
                child: const Text("Cargando..."),
              ),
            ],
          ),
        );
      });
}

showAlertDialog(BuildContext context) {
  showDialog(
      barrierDismissible: false,
      context: context,
      builder: (BuildContext context) {
        return AlertDialog(
          content: Row(
            children: [
              const CircularProgressIndicator(),
              Container(
                margin: const EdgeInsets.only(left: 35),
                child: const Text("Cerrando sesión..."),
              ),
            ],
          ),
        );
      });
}
const espacio = SizedBox(
  width: 25,
  height: 25,
);
const espacioCH = SizedBox(
  width: 10,
  height: 10,
);

var tema = Colors.blueGrey.shade700;

var logo = SizedBox(
  height: alto * 0.2,
  child: Image.asset(logoDir),
);

const titulos = TextStyle(
  fontWeight: FontWeight.w300,
  fontSize: 25,
);

const titulos6 = TextStyle(
  fontWeight: FontWeight.bold,
  shadows: 
  [Shadow(
    offset: Offset(2, 2), 
    blurRadius: 5,
    color: Colors.white,
    ),
    Shadow(
    offset: Offset(-2, -2), 
    blurRadius: 5,
    color: Colors.white,
    ),
    ],
  
  fontSize: 25,
);

const titulos2 = TextStyle(
  fontWeight: FontWeight.w300,
  fontSize: 15,
);

var titulos3 = TextStyle(
  fontWeight: FontWeight.bold,
  color: tema,
  fontSize: 12,
);

var titulos4 = TextStyle(
  fontWeight: FontWeight.w500,
  color: tema,
  fontSize: 12,
);

var titulos5 = TextStyle(
  fontStyle: FontStyle.italic,
  fontWeight: FontWeight.w300,
  color: tema,
  fontSize: 15,
);

var titulos5x = const TextStyle(
  fontStyle: FontStyle.italic,
  color: Colors.black,
  fontSize: 15,
);

var titulos5y = const TextStyle(
  fontStyle: FontStyle.italic,
  fontWeight: FontWeight.bold,
  color: Colors.black,
  fontSize: 15,
);

var titulos5z = const TextStyle(
  fontStyle: FontStyle.italic,
  color: Colors.black,
  fontSize: 18,
);

var textlist = const TextStyle(
  fontWeight: FontWeight.bold,
  fontSize: 20,
);

var titulos7 = TextStyle(
  fontWeight: FontWeight.bold,
  color: tema,
  fontSize: 12,
);

var logoInicio = Padding(
  padding: EdgeInsets.fromLTRB(10, alto * 0.2, 10, 10),
  child: logo,
);

var buttonstyle = ElevatedButton.styleFrom(
  primary: tema,
  elevation: 10.0,
  textStyle: const TextStyle(
    fontSize: 20,
  ),
);

class Widgets {
  formularioLogin(
      {required context,
      required tipoTeclado,
      required ocultar,
      required icono,
      required accionEnter,
      required validator,
      required controlador,
      required pista,
      required etiqueta}) {
    return Padding(
      padding: const EdgeInsets.fromLTRB(20, 5, 20, 5),
      child: TextFormField(
        keyboardType: tipoTeclado,
        obscureText: ocultar,
        textInputAction: accionEnter,
        controller: controlador,
        validator: validator,
        decoration: InputDecoration(
          icon: Icon(icono),
          hintText: pista,
          labelText: etiqueta,
        ),
      ),
    );
  }

  toast(context, text, color) {
    return ScaffoldMessenger.of(context).showSnackBar(SnackBar(
        behavior: SnackBarBehavior.floating,
        backgroundColor: color,
        duration: const Duration(seconds: 5), 
        content: Text(text, 
        style: const TextStyle(
          fontSize: 22,
        ))));
  }
}
