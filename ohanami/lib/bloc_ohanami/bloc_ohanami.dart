import 'package:ohanami/app/componentes_y_estilos.dart';
import 'package:ohanami/bloc_ohanami/estados.dart';
import 'package:ohanami/bloc_ohanami/eventos.dart';
import 'package:ohanami/vistas/exports/export_inicial.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:bloc/bloc.dart';
import 'package:db_paquete/db_paquete.dart';
import 'package:flutter_sesion/flutter_session.dart';
import 'package:partida/partida.dart';

late Partida partida;
late Usuario usuario;

class OhanamiBloc extends Bloc<Evento, Estado2> {
  OhanamiBloc() : super(const Iniciando()) {
    on<VerificarUsuario>(_conexion);
    on<Desconectarse>(_cerrarsesion);
    on<Registrar>((event, emit) => emit(const NuevoRegistro()));
    on<CheckRED>((event, emit) => emit(const DesconectadoBDD()));
    on<IniciarRonda1>((event, emit) => emit(Ronda1(event.partida)));
    on<ContinuarRonda2>((event, emit) => emit(Ronda2(event.partida)));
    on<ContinuarRonda3>((event, emit) => emit(Ronda3(event.partida)));
    on<AgregarNuevaPartida>((event, emit) => emit(const AgregarPartida()));
    on<MostrarListas>((event, emit) => emit(ObtenerListas(event.usuario)));
    on<VerDetallesPartida>(
        (event, emit) => emit(DetallesPartida(event.partida, event.usuario)));

    add(VerificarUsuario());
  }

  _cerrarsesion(Evento event, Emitter<Estado2> emit) async {
    await FlutterSession().set("datos", "");
    await FlutterSession().set("tipoConexion", "");
    await FlutterSession().set("usuario", "");
  }

  _conexion(Evento event, Emitter<Estado2> emit) async {
    if (await FlutterSession().get("tipoConexion") == 0) {
      Usuario us = await usuarioLocal.recuperarUsuario();
      return emit(ObtenerListas(us));
    }

    if (await FlutterSession().get("tipoConexion") == 1) {
      if (await usuarioRemoto.checarRed() == true) {
        Usuario us = await usuarioRemoto
            .recuperarUsuario(await FlutterSession().get("usuario"));
        return emit(ObtenerListas(us));
      } else {
        return emit(const DesconectadoBDD());
      }
    }
    emit(const ConectadoBDD());
  }
}
