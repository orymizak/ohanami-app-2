import 'package:db_paquete/db_paquete.dart';
import 'package:freezed_annotation/freezed_annotation.dart';
import 'package:partida/partida.dart';

part 'estados.freezed.dart';

@freezed
class Estado2 with _$Estado2 {
  const factory Estado2() = Iniciando;
  const factory Estado2.nReg() = NuevoRegistro;
  const factory Estado2.conB() = ConectadoBDD;
  const factory Estado2.desc() = DesconectadoBDD;
  const factory Estado2.agPa() = AgregarPartida;
  const factory Estado2.ron1(Partida partida) = Ronda1;
  const factory Estado2.ron2(Partida partida) = Ronda2;
  const factory Estado2.ron3(Partida partida) = Ronda3;
  const factory Estado2.verL(Usuario usuario) = ObtenerListas;
  const factory Estado2.verD(Partida partida, Usuario usuario) = DetallesPartida;
}
