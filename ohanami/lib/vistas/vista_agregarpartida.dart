import 'package:flutter_sesion/flutter_session.dart';
import 'exports/exports.dart';

class NuevaPartida extends StatefulWidget {
  const NuevaPartida({Key? key}) : super(key: key);

  @override
  _NuevaPartidaState createState() => _NuevaPartidaState();
}

class _NuevaPartidaState extends State<NuevaPartida> {
  late bool agregar, eliminar, comenzar;
  late int _contador;
  late Partida partida;
  late Usuario usuario;
  final _control = GlobalKey<FormState>();

  final List<TextEditingController> _lista =
      List.generate(maxJugadores, (i) => TextEditingController());

  @override
  void initState() {
    super.initState();
    getUser();
    _contador = 2;
    eliminar = false;
    agregar = true;
    comenzar = false;
  }

  void validarJugadores() {
    try {
      Set<Jugador> jugadores = {};
      for (var i = 0; i < _contador; i++) {
        jugadores.add(Jugador(nombre: _lista[i].text));
      }
      if (jugadores.length != _contador) throw ProblemaJugadoresRepetidos();

      Partida partida = Partida(jugadores: jugadores);
      context.read<OhanamiBloc>().add(IniciarRonda1(partida: partida));
    } on Exception catch (e) {
      widget.toast(context, e.toString(), Colors.red);
    }
  }

  void getUser() async {
    var usuario = await FlutterSession().get("usuario");
    setState(() {
      _lista[0] = TextEditingController()..text = usuario.toString();
    });
  }

  void verificadorLista() {
    bool check = true;
    for (var i = 0; i < _contador; i++) {
      if (_lista[i].text.isEmpty) {
        check = false;
      }
    }
    setState(() {
      comenzar = check;
    });
  }

  bool btn1 = true;
  bool btn2 = true;
  bool btn3 = false;

  _floatingActionButtons() {
    return Padding(
      padding: const EdgeInsets.only(bottom: 15),
      child: Row(
        mainAxisAlignment: MainAxisAlignment.center,
        children: <Widget>[
          Opacity(
            opacity: _contador == 4 ? 0.2 : 1,
            child: _floatingbtn(Icons.add, Colors.blue, _contador < 4 ? true : false, () {
              if (_contador < 4) {
                setState(() {
                  _contador++;
                  comenzar = false;
                });
              }
            }),
          ),
          espacioCH,
          Opacity(
              opacity: _contador == 2 ? 0.2 : 1,
              child: _floatingbtn(Icons.remove, Colors.red, _contador > 2 ? true : false, () {
                if (_contador > 2) {
                  _lista[_contador - 1].clear();
                  setState(() {
                    _contador--;
                  });
                  verificadorLista();
                }
              })),
          espacioCH,
          _floatingstart(),
          // botones()
        ],
      ),
    );
  }

  _floatingbtn(icon, color, check, func) {
    return FloatingActionButton.extended(
      enableFeedback: check,
      
      backgroundColor: color,
      label: Icon(icon),
      onPressed: func,
    );
  }

  _floatingstart() {
    return FloatingActionButton.extended(
      backgroundColor: Colors.green,
      label: Row(
        children: const [
          Text("Comenzar",
              style: TextStyle(fontWeight: FontWeight.bold, fontSize: 20)),
          espacioCH,
          Icon(Icons.arrow_right_alt),
        ],
      ),
      onPressed: () {
        if (_control.currentState!.validate()) {
          validarJugadores();
        }
      },
    );
  }

  @override
  Widget build(BuildContext context) {
    WidgetsFlutterBinding.ensureInitialized();
    return Scaffold(
      floatingActionButtonLocation: FloatingActionButtonLocation.centerDocked,
      floatingActionButton: _floatingActionButtons(),
      appBar: AppBar(
        backgroundColor: tema,
        centerTitle: true,
        leading: IconButton(
          icon: const Icon(Icons.arrow_back, color: Colors.white),
          onPressed: () async => context.read<OhanamiBloc>().add(MostrarListas(
              Usuario.fromJson(await FlutterSession().get("datos")))),
        ),
        title: const Text("Agregar partida"),
      ),
      body: Scrollbar(
        radius: const Radius.circular(5),
        thickness: 8,
        isAlwaysShown: true,
        child: SingleChildScrollView(
          physics: const BouncingScrollPhysics(),
          child: Container(
              alignment: Alignment.topCenter,
              padding: const EdgeInsets.fromLTRB(15, 15, 15, 150),
              child: Form(
                key: _control,
                child: Column(
                  children: [
                    _row(0, false),
                    ListView.builder(
                        physics: const BouncingScrollPhysics(),
                        shrinkWrap: true,
                        itemCount: _contador - 1,
                        itemBuilder: (context, index) {
                          return _row(index + 1, true);
                        }),
                  ],
                ),
              )),
        ),
      ),
    );
  }

  _row(int index, focus) {
    return Row(
      children: [
        SizedBox(
          height: 50,
          child: Text('Jugador ' + (index + 1).toString() + ':',
              style: const TextStyle(fontSize: 18)),
        ),
        const SizedBox(width: 30),
        Expanded(
            child: Container(
          padding: const EdgeInsets.all(5),
          child: TextFormField(
              maxLength: 8,
              validator: (value) {
                if (value == null || value.length < 5) {
                  return 'Ingresa al menos 5 caracteres';
                }
                return null;
              },
              textInputAction: TextInputAction.next,
              autofocus: focus,
              buildCounter: (
                BuildContext context, {
                required int currentLength,
                int? maxLength,
                required bool isFocused,
              }) =>
                  null,
              style: const TextStyle(fontSize: 20),
              decoration: const InputDecoration(
                hintText: "Mín. 5 caracteres",
                border: OutlineInputBorder(),
              ),
              controller: _lista[index],
              onChanged: (_) => verificadorLista()),
        )),
      ],
    );
  }
}
