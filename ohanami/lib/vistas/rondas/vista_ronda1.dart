import 'package:flutter_sesion/flutter_session.dart';
import 'package:ohanami/vistas/exports/export_rondas.dart';

class VistaRonda1 extends StatefulWidget {
  const VistaRonda1({Key? key, required this.partida}) : super(key: key);
  final Partida partida;

  @override
  _VistaRonda1State createState() => _VistaRonda1State(partida);
}

class _VistaRonda1State extends State<VistaRonda1> {
  final List<TextEditingController> _cartasAzules = [];
  Partida partida;

  _VistaRonda1State(this.partida);

  void tipoConexion() async {
    if (await FlutterSession().get("tipoConexion") == 0) {
      context
          .read<OhanamiBloc>()
          .add(MostrarListas(await usuarioLocal.recuperarUsuario()));
    }
    if (await FlutterSession().get("tipoConexion") == 1) {
      showAlertDialog3(context);
      if (await usuarioRemoto.checarRed() == true) {
      usuario = await usuarioRemoto
          .recuperarUsuario(await FlutterSession().get("usuario"));
      Navigator.pop(context);
      context.read<OhanamiBloc>().add(MostrarListas(usuario));
      }
      Navigator.pop(context);
      widget.toast(
          context,
          "Error: no se pudo establecer conexión con la base de datos.",
          Colors.red);
    }
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      floatingActionButtonLocation: FloatingActionButtonLocation.centerDocked,
      floatingActionButton: _floatingRondas(),
      appBar: AppBar(
        backgroundColor: tema,
        title: const Text("Ronda 1"),
        centerTitle: true,
        leading: IconButton(
            icon: const Icon(Icons.arrow_back, color: Colors.white),
            onPressed: () => tipoConexion()),
      ),
      body: Scrollbar(
        thickness: 8,
        isAlwaysShown: true,
        radius: const Radius.circular(5),
        child: Center(
          child: _listView(),
        ),
      ),
    );
  }

  _validarNumeroDeCartas() {
    try {
      List<CartasRonda1> lista = [];
      for (var i = 0; i < partida.jugadores.length; i++) {
        int azules = int.parse(_cartasAzules[i].text);
        CartasRonda1 cr1 = CartasRonda1(
          jugador: partida.jugadores.elementAt(i),
          cuantasAzules: azules,
        );
        lista.add(cr1);
      }
      partida.cartasronda1(lista);
      context.read<OhanamiBloc>().add(ContinuarRonda2(partida: partida));
    } on Exception catch (e) {
      if (e.runtimeType == FormatException) {
        return widget.toast(
            context, "Llene los campos correctamente.", Colors.red);
      }
      widget.toast(context, e.toString(), Colors.red);
    }
  }

  _floatingRondas() {
    return Padding(
      padding: const EdgeInsets.only(bottom: 10),
      child: FloatingActionButton.extended(
        backgroundColor: Colors.green,
        label: Row(
          children: const [
            Text("Siguiente ronda",
                style: TextStyle(fontWeight: FontWeight.bold, fontSize: 20)),
            espacioCH,
            Icon(Icons.arrow_right_alt),
          ],
        ),
        onPressed: () {
          _validarNumeroDeCartas();
        },
      ),
    );
  }

  _listView() {
    return ListView.builder(
        itemCount: partida.jugadores.length + 1,
        itemBuilder: (BuildContext context, int index) {
          return partida.jugadores.length == index
              ? const SizedBox(height: 180)
              : Padding(
                  padding: const EdgeInsets.all(5.0),
                  child: Container(
                    decoration: BoxDecoration(
                      borderRadius: const BorderRadius.all(Radius.circular(5)),
                      color: Colors.blue.shade100,
                    ),
                    child: Column(
                      children: [
                        SizedBox(
                          height: 190.0,
                          child: Column(
                            crossAxisAlignment: CrossAxisAlignment.start,
                            children: [
                              espacioCH,
                              _cartas(),
                              Row(
                                mainAxisAlignment: MainAxisAlignment.center,
                                children: [
                                  _input(_cartasAzules, index),
                                  _inputFake(),
                                  _inputFake(),
                                  _inputFake(),
                                ],
                              ),
                              espacioCH,
                              Text(
                                  "  Jugador " +
                                      (index + 1).toString() +
                                      ": " +
                                      partida.jugadores
                                          .elementAt(index)
                                          .nombre
                                          .toString() +
                                      ".",
                                  style: titulos5x),
                            ],
                          ),
                        ),
                      ],
                    ),
                  ));
        });
  }

  _imagenCarta(src) {
    return SizedBox(width: 72, child: Image.asset(src));
  }

  _cartas() {
    return Row(
      mainAxisAlignment: MainAxisAlignment.center,
      children: [
        _imagenCarta("lib/src/cartaazul.png"),
        Opacity(opacity: 0.1, child: _imagenCarta("lib/src/cartaverde.png")),
        Opacity(opacity: 0.1, child: _imagenCarta("lib/src/cartarosa.png")),
        Opacity(opacity: 0.1, child: _imagenCarta("lib/src/cartanegra.png")),
      ],
    );
  }

  _inputFake() {
    return Container(
      width: 72,
    );
  }

  _input(_var, index) {
    _var.add(TextEditingController());
    return Container(
      padding: const EdgeInsets.fromLTRB(8, 12, 8, 8),
      width: 72,
      height: 72,
      child: TextFormField(
        onChanged: (_) {},
        buildCounter: (
          BuildContext context, {
          required int currentLength,
          int? maxLength,
          required bool isFocused,
        }) =>
            null,
        controller: _var[index],
        maxLength: 2,
        autofocus: true,
        inputFormatters: <TextInputFormatter>[
          FilteringTextInputFormatter.allow(RegExp(r'[0-9]')),
        ],
        keyboardType: TextInputType.number,
        textAlignVertical: TextAlignVertical.bottom,
        textInputAction: TextInputAction.next,
        style: const TextStyle(fontSize: 25),
        textAlign: TextAlign.center,
        decoration: InputDecoration(
          hintText: "?",
          hintStyle: TextStyle(
            color: Colors.red.shade200,
            fontStyle: FontStyle.italic,
          ),
          fillColor: Colors.white,
          filled: true,
          border: const OutlineInputBorder(),
        ),
      ),
    );
  }
}
