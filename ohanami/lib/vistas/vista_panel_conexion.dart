import 'exports/export_panel.dart';

class VistaConectado extends StatefulWidget {
  const VistaConectado({Key? key}) : super(key: key);

  @override
  _VistaConectadoState createState() => _VistaConectadoState();
}

bool btnConectarse = false;
TextEditingController _campoLogin = TextEditingController(),
    _password = TextEditingController();

class _VistaConectadoState extends State<VistaConectado> {
  bool botonclic = true;
  final _formKey = GlobalKey<FormState>();

  @override
  void initState() {
    _campoLogin = TextEditingController();
    _password = TextEditingController();
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: SingleChildScrollView(
        reverse: true,
        child: Container(
          alignment: Alignment.center,
          child: Form(
            key: _formKey,
            child: Column(
              mainAxisAlignment: MainAxisAlignment.center,
              children: [
                logoInicio,
                const Text("Bienvenido a Ohanami", style: titulos),
                espacio,
                widget.formularioLogin(
                    context: context,
                    tipoTeclado: TextInputType.emailAddress,
                    ocultar: false,
                    controlador: _campoLogin,
                    validator: (value) {
                      if (value == null || value.toString().length < 5) {
                        return 'Ingrese un usuario válido';
                      }
                      return null;
                    },
                    icono: Icons.account_circle,
                    accionEnter: TextInputAction.next,
                    pista: 'Ingrese su nombre de usuario',
                    etiqueta: 'Usuario'),
                widget.formularioLogin(
                    context: context,
                    tipoTeclado: TextInputType.visiblePassword,
                    ocultar: true,
                    controlador: _password,
                    validator: (value) {
                      if (value == null || value.toString().length < 5) {
                        return 'Ingrese una contraseña válida';
                      }
                      return null;
                    },
                    icono: Icons.lock,
                    accionEnter: TextInputAction.go,
                    pista: 'Ingrese su contraseña',
                    etiqueta: 'Contraseña'),
                espacio,
                btnConectarse == true
                    ? const CircularProgressIndicator()
                    : Opacity(
                        opacity: botonclic ? 1.0 : 0.2,
                        child: ElevatedButton(
                            onPressed: () => _modoRemoto(_formKey),
                            child: Container(
                              padding: const EdgeInsets.all(5.0),
                              child: const Text(
                                'Conectarse',
                                style: TextStyle(
                                  fontSize: 16.0,
                                  fontWeight: FontWeight.w400,
                                ),
                              ),
                            ),
                            style: buttonstyle),
                      ),
                espacio,
                Row(
                  mainAxisAlignment: MainAxisAlignment.center,
                  children: [
                    const Text("¿Aún no tienes cuenta? ", style: titulos2),
                    TextButton(
                      child: Text("¡Regístrate!", style: titulos3),
                      style: ButtonStyle(
                        overlayColor:
                            MaterialStateProperty.all(Colors.transparent),
                      ),
                      onPressed: () {
                        context.read<OhanamiBloc>().add(Registrar());
                      },
                    ),
                  ],
                ),
                TextButton(
                  child: Text("Jugar como invitado", style: titulos4),
                  style: ButtonStyle(
                    overlayColor: MaterialStateProperty.all(Colors.transparent),
                  ),
                  onPressed: () {
                    _modoLocal();
                  },
                ),
                espacioCH,
              ],
            ),
          ),
        ),
      ),
    );
  }

  void _modoLocal() async {
    Usuario datos =
        Usuario(usuario: "usuario", telefono: 0, password: "", partidas: []);
    await usuarioLocal.registrarUsuario(usuario: datos);
    context.read<OhanamiBloc>().add(MostrarListas(datos));
  }

  void _modoRemoto(_formKey) async {
    if (_formKey.currentState!.validate()) {
      setState(() {
        btnConectarse = true;
      });
      Usuario us = Usuario(
          usuario: _campoLogin.text,
          telefono: 0,
          password: _password.text,
          partidas: []);

      if (await usuarioRemoto.checarRed() == true) {
        if (await usuarioRemoto.verificarSesion(usuarioRemoto: us) == true) {
          Usuario us = await usuarioRemoto.recuperarUsuario(_campoLogin.text);
          widget.toast(
              context, "¡Bienvenido " + us.usuario + "!", Colors.green);
          context.read<OhanamiBloc>().add(MostrarListas(us));
          return;
        }
        setState(() {
          btnConectarse = false;
        });
        widget.toast(context, "No se pudo iniciar sesión, verifique sus datos.",
            Colors.red);
        return;
      }
      context.read<OhanamiBloc>().add(CheckRED());
    }
  }
}
