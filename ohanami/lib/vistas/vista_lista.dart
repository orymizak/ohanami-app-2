import 'package:intl/intl.dart';
import 'package:flutter/material.dart';
import 'package:flutter_sesion/flutter_session.dart';
import 'exports/export_lista.dart';

class ListaPartida extends StatefulWidget {
  const ListaPartida({Key? key, required this.usuario}) : super(key: key);
  final Usuario usuario;

  @override
  _ListaPartidaState createState() => _ListaPartidaState(usuario);
}

class _ListaPartidaState extends State<ListaPartida> {
  final _control = GlobalKey<FormState>();
  final TextEditingController _text = TextEditingController(),
      _usuario = TextEditingController(),
      _telefono = TextEditingController(),
      _contra = TextEditingController(),
      _contra2 = TextEditingController();

  var cadena = usuarioLocal.recuperarUsuario();
  final Usuario usuario;
  late Future<Usuario> us;
  bool isLocal = true;

  _ListaPartidaState(this.usuario);

  @override
  void initState() {
    Future.delayed(Duration.zero, () async {
      if (await FlutterSession().get("tipoConexion") == 1) {
        cadena = usuarioRemoto
            .recuperarUsuario(await FlutterSession().get("usuario"));
        isLocal = false;
      }
    });
    us = cadena;
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        backgroundColor: tema,
        centerTitle: true,
        title: const Text("Lista de partidas"),
      ),
      drawer: _drawer(widget),
      floatingActionButtonLocation: FloatingActionButtonLocation.centerFloat,
      floatingActionButton: _floatingActionButton(),
      body: Center(
        child: Column(
          mainAxisAlignment: MainAxisAlignment.center,
          children: [
            Flexible(
              child: _listView(),
            ),
          ],
        ),
      ),
    );
  }

  _listView() {
    return usuario.partidas.isEmpty
        ? const Text("Está muy vacío aquí. \nAgrega una nueva partida.",
            textAlign: TextAlign.center)
        : Scrollbar(
            thickness: 8,
            radius: const Radius.circular(5),
            isAlwaysShown: true,
            child: ListView.builder(
                itemCount: usuario.partidas.length,
                itemBuilder: (context, index) {
                  List<String> nombres = [];
                  int indexInv = usuario.partidas.length - 1 - index;
                  var fecha = usuario.partidas[indexInv].fecha;
                  var usPart = usuario.partidas[indexInv];
                  for (var i = 0; i < usPart.jugadores.length; i++) {
                    nombres.add((i + 1).toString() +
                        ". " +
                        usPart.jugadores.elementAt(i).nombre.toString());
                  }
                  do {
                    nombres.add("");
                  } while (nombres.length < 4);
                  return _inkWell(usPart, nombres, fecha, indexInv, index);
                }),
          );
  }

  _inkWell(usPart, nombres, fecha, indexInv, index) {
    return InkWell(
      onTap: () {
        context
            .read<OhanamiBloc>()
            .add(VerDetallesPartida(partida: usPart, usuario: usuario));
      },
      child: Container(
          padding: const EdgeInsets.fromLTRB(5, 10, 5, 5),
          margin: const EdgeInsets.fromLTRB(8, 8, 8, 4),
          decoration: BoxDecoration(
            borderRadius: const BorderRadius.all(Radius.circular(5)),
            color: Colors.grey.shade300,
          ),
          height: 128,
          child: Row(
            mainAxisAlignment: MainAxisAlignment.spaceBetween,
            children: [
              _listaJugadores(nombres),
              Column(
                crossAxisAlignment: CrossAxisAlignment.end,
                children: [
                  _cartas(),
                  _cantidadCartas(usPart, nombres),
                  _borrar(usPart, indexInv, fecha, widget),
                ],
              )
            ],
          )),
    );
  }

  _borrar(usPart, indexInv, fecha, widget) {
    DateFormat dateFormat = DateFormat("dd/MM/yyyy - HH:mm");
    String string = dateFormat.format(fecha);
    return Row(
      children: [
        // interprete
        Text(string.toString() + " h."),
        MaterialButton(
          height: 10,
          minWidth: 10,
          highlightColor: Colors.transparent,
          splashColor: Colors.transparent,
          textColor: Colors.red,
          child: const Icon(Icons.delete_forever),
          onPressed: () async {
            showDialog(
              context: context,
              builder: (context) => AlertDialog(
                title: const Text("Eliminar partida"),
                content: const Text("¿Desea esta partida?"),
                actions: [
                  TextButton(
                      onPressed: () {
                        Navigator.pop(context);
                      },
                      child: const Text("No")),
                  ElevatedButton(
                      onPressed: () async {
                        await _eliminarFnc(widget, indexInv);
                      },
                      child: const Text("Sí")),
                ],
              ),
            );
          },
        ),
      ],
    );
  }

  _eliminarFnc(widget, indexInv) async {
    if (await FlutterSession().get("tipoConexion") == 0) {
      usuarioLocal.eliminarPartida(indice: indexInv).whenComplete(
          () => setState(() => usuario.partidas.removeAt(indexInv)));
      widget.toast(context, "Partida eliminada.", Colors.orange);
      return Navigator.pop(context);
    }
    if (await FlutterSession().get("tipoConexion") == 1) {
      await usuarioRemoto
          .eliminarPartida(indice: indexInv)
          .whenComplete(() => setState(() {
                usuario.partidas.removeAt(indexInv);
                Navigator.pop(context);
                widget.toast(context, "Partida eliminada.", Colors.orange);
                return;
              }))
          .onError((error, stackTrace) =>
              widget.toast(context, "No se pudo eliminar.", Colors.red));
    }
  }

  _listaJugadores(nombres) {
    return Column(crossAxisAlignment: CrossAxisAlignment.start, children: [
      Text('JUGADORES', style: titulos5x),
      Text(nombres[0]),
      Text(nombres[1]),
      Text(nombres[2]),
      Text(nombres[3]),
    ]);
  }

  _cantidadCartas(usPart, jugador) {
    List<PuntuacionJugador> j;
    Partida p = usPart;
    p.puntos(ronda: FasePuntuacion.ronda1);
    p.puntos(ronda: FasePuntuacion.ronda2);
    p.puntos(ronda: FasePuntuacion.ronda3);
    j = p.puntos(ronda: FasePuntuacion.desenlace);

    return Padding(
      padding: const EdgeInsets.only(top: 5),
      child: Row(
        children: [
          _cartaIndividual(j[0].porAzules),
          _cartaIndividual(j[0].porVerdes),
          _cartaIndividual(j[0].porRosas),
          _cartaIndividual(j[0].porNegras),
          espacioCH,
          espacioCH,
          _cartaTotal(j[0].total),
        ],
      ),
    );
  }

  _cartaIndividual(text) {
    return SizedBox(
      width: 40,
      child: Text(
        text.toString(),
        textAlign: TextAlign.center,
        style: titulos5x,
      ),
    );
  }

  _cartaTotal(text) {
    return SizedBox(
      width: 40,
      child: Text(
        text.toString(),
        textAlign: TextAlign.center,
        style: titulos5y,
      ),
    );
  }

  _cartas() {
    return Row(
      children: [
        _imagenCarta("lib/src/cartaazul.png"),
        _imagenCarta("lib/src/cartaverde.png"),
        _imagenCarta("lib/src/cartarosa.png"),
        _imagenCarta("lib/src/cartanegra.png"),
        espacioCH,
        espacioCH,
        _imagenCarta("lib/src/cartas.png"),
      ],
    );
  }

  _imagenCarta(src) {
    return SizedBox(width: 40, child: Image.asset(src));
  }

  _drawer(widget) {
    return Drawer(
        child: ListView(padding: EdgeInsets.zero, children: [
      DrawerHeader(
        decoration: const BoxDecoration(
            color: Colors.blue,
            image: DecorationImage(
                fit: BoxFit.fill, image: AssetImage("lib/src/drawer.png"))),
        child: FutureBuilder(
            future: FlutterSession().get("usuario"),
            builder: (context, snapshot) {
              return snapshot.hasData
                  ? snapshot.hasError
                      ? const Text("Sesión expirada (1)", style: titulos6)
                      : Text("¡Bienvenido, " + snapshot.data.toString() + "!",
                          style: titulos6)
                  : Column(children: [
                      const SizedBox(height: 120),
                      Container(
                        margin: const EdgeInsets.fromLTRB(20, 0, 20, 0),
                        child: const LinearProgressIndicator(),
                      )
                    ]);
            }),
      ),
      FutureBuilder(
          future: FlutterSession().get("tipoConexion"),
          builder: (context, snapshot) {
            if (snapshot.data == 0) {
              return _elementoLista("Cambiar nombre por defecto", Icons.person,
                  () {
                Navigator.pop(context);
                showDialog(
                  context: context,
                  builder: (context) => AlertDialog(
                    title: const Text("Nuevo nombre de usuario"),
                    content: const Text(
                        "Ingresa tu nombre de usuario predeterminado.\n\nSi es temporal, puedes modificado al iniciar una partida."),
                    actions: [
                      Form(
                        key: _control,
                        child: Column(children: [
                          Padding(
                            padding: const EdgeInsets.fromLTRB(30, 0, 30, 10),
                            child: TextFormField(
                                autofocus: true,
                                controller: _text,
                                maxLength: 8,
                                validator: (value) {
                                  if (value == null || value.length < 5) {
                                    return 'Ingresa al menos 5 caracteres';
                                  }
                                  return null;
                                },
                                decoration: const InputDecoration(
                                    hintText: "Mín. 5 caracteres")),
                          ),
                          Row(
                            mainAxisAlignment: MainAxisAlignment.end,
                            children: [
                              TextButton(
                                  onPressed: () {
                                    Navigator.pop(context);
                                  },
                                  child: const Text("CANCELAR")),
                              espacioCH,
                              ElevatedButton(
                                  onPressed: () async {
                                    if (_control.currentState!.validate()) {
                                      await FlutterSession()
                                          .set("usuario", _text.text);
                                      await usuarioLocal
                                          .actualizarNombre(_text.text);
                                      Navigator.pop(context);
                                      widget.toast(
                                          context,
                                          "Tu nombre de usuario se ha actualizado. Ahora es " +
                                              _text.text +
                                              ".",
                                          Colors.blue);
                                    }
                                  },
                                  child: const Text("CAMBIAR")),
                            ],
                          ),
                        ]),
                      ),
                    ],
                  ),
                );
              });
            }
            return Container();
          }),
      FutureBuilder(
          future: FlutterSession().get("tipoConexion"),
          builder: (context, snapshot) {
            if (snapshot.data == 0) {
              return _elementoLista("Registrarse", Icons.backup, () {
                Navigator.pop(context);
                showDialog(
                  context: context,
                  builder: (context) => SingleChildScrollView(
                    physics: const BouncingScrollPhysics(),
                    child: AlertDialog(
                      title: const Text("Registro de nuevo usuario"),
                      content: const Text("Llena el siguiente formulario."),
                      actions: [
                        Form(
                          key: _control,
                          child: Column(children: [
                            Padding(
                              padding: const EdgeInsets.fromLTRB(30, 0, 30, 10),
                              child: TextFormField(
                                  autofocus: true,
                                  controller: _usuario,
                                  maxLength: 8,
                                  inputFormatters: <TextInputFormatter>[
                                    FilteringTextInputFormatter.allow(
                                        RegExp(r'[A-Za-z]')),
                                  ],
                                  keyboardType: TextInputType.name,
                                  buildCounter: (
                                    BuildContext context, {
                                    required int currentLength,
                                    int? maxLength,
                                    required bool isFocused,
                                  }) =>
                                      null,
                                  validator: (value) {
                                    if (value == null ||
                                        value.length < 3 ||
                                        value.length > 8) {
                                      return 'Ingresa de 3 a 8 caracteres';
                                    }
                                    return null;
                                  },
                                  decoration: const InputDecoration(
                                      labelText: "Nombre de usuario",
                                      border: OutlineInputBorder(),
                                      hintText: "5 - 8 caracteres")),
                            ),
                            espacioCH,
                            Padding(
                              padding: const EdgeInsets.fromLTRB(30, 0, 30, 10),
                              child: TextFormField(
                                  autofocus: true,
                                  controller: _contra,
                                  obscureText: true,
                                  keyboardType: TextInputType.visiblePassword,
                                  buildCounter: (
                                    BuildContext context, {
                                    required int currentLength,
                                    int? maxLength,
                                    required bool isFocused,
                                  }) =>
                                      null,
                                  validator: (value) {
                                    if (value == null || value.length < 7) {
                                      return 'Ingresa al menos 7 caracteres';
                                    }
                                    return null;
                                  },
                                  decoration: const InputDecoration(
                                      labelText: "Contraseña",
                                      border: OutlineInputBorder(),
                                      hintText: "Mín. 7 caracteres")),
                            ),
                            Padding(
                              padding: const EdgeInsets.fromLTRB(30, 0, 30, 10),
                              child: TextFormField(
                                  autofocus: true,
                                  controller: _contra2,
                                  keyboardType: TextInputType.visiblePassword,
                                  obscureText: true,
                                  buildCounter: (
                                    BuildContext context, {
                                    required int currentLength,
                                    int? maxLength,
                                    required bool isFocused,
                                  }) =>
                                      null,
                                  validator: (value) {
                                    if (value == null || value.length < 7) {
                                      return 'Repite tu contraseña';
                                    }
                                    if (value != _contra.text) {
                                      return 'Las contraseñas no coinciden';
                                    }
                                    return null;
                                  },
                                  decoration: const InputDecoration(
                                      labelText: "Repite tu contraseña",
                                      border: OutlineInputBorder(),
                                      hintText: "Mín. 7 caracteres")),
                            ),
                            Padding(
                              padding: const EdgeInsets.fromLTRB(30, 0, 30, 10),
                              child: TextFormField(
                                  autofocus: true,
                                  controller: _telefono,
                                  inputFormatters: <TextInputFormatter>[
                                    FilteringTextInputFormatter.allow(
                                        RegExp(r'[0-9]')),
                                  ],
                                  maxLength: 10,
                                  keyboardType: TextInputType.number,
                                  buildCounter: (
                                    BuildContext context, {
                                    required int currentLength,
                                    int? maxLength,
                                    required bool isFocused,
                                  }) =>
                                      null,
                                  validator: (value) {
                                    if (value == null || value.length != 10) {
                                      return 'Verifica que sean 10 dígitos';
                                    }
                                    return null;
                                  },
                                  decoration: const InputDecoration(
                                      labelText: "Ingresa tu teléfono",
                                      border: OutlineInputBorder(),
                                      hintText: "Ingresa 10 dígitos")),
                            ),
                            Row(
                              mainAxisAlignment: MainAxisAlignment.center,
                              children: [
                                TextButton(
                                    onPressed: () async {
                                      Navigator.of(context).pop();
                                    },
                                    child: const Text("CANCELAR")),
                                espacioCH,
                                ElevatedButton(
                                    onPressed: () async {
                                      if (_control.currentState!.validate()) {
                                        Usuario usuario = Usuario.fromJson(
                                            await FlutterSession()
                                                .get("datos"));
                                        Usuario us = Usuario(
                                            usuario: _usuario.text,
                                            telefono: int.parse(_telefono.text),
                                            password: _contra.text,
                                            partidas: usuario.partidas);
                                        await FlutterSession()
                                            .set("datos", us)
                                            .then((value) => true);

                                        showAlertDialog2(context);

                                        if (await usuarioRemoto.checarRed() ==
                                            true) {
                                          if (await usuarioRemoto
                                                  .registradoUsuario(
                                                      usuarioRemoto: us) ==
                                              true) {
                                                
                                                                      Navigator.of(
                                                                              context)
                                                                          .pop();
                                            showDialog(
                                              context: context,
                                              builder: (context) => Center(
                                                child: SingleChildScrollView(
                                                  physics:
                                                      const BouncingScrollPhysics(),
                                                  child: AlertDialog(
                                                    title: const Text(
                                                        "Usuario registrado"),
                                                    content: const Text(
                                                        "El usuario ya se encuentra registrado."),
                                                    actions: [
                                                      Column(children: [
                                                        Row(
                                                          mainAxisAlignment:
                                                              MainAxisAlignment
                                                                  .center,
                                                          children: [
                                                            ElevatedButton(
                                                                onPressed: () {
                                                                  Navigator.of(
                                                                          context)
                                                                      .pop();
                                                                },
                                                                child:
                                                                    const Text(
                                                                        "OK")),
                                                          ],
                                                        ),
                                                      ]),
                                                    ],
                                                  ),
                                                ),
                                              ),
                                            );
                                            return;
                                          }
                                          await usuarioRemoto
                                              .registrarUsuario(
                                                  usuarioRemoto: us)
                                              .then((value) {
                                                                  Navigator.of(
                                                                          context)
                                                                      .pop();
                                            widget.toast(
                                                context,
                                                "¡Registro exitoso! Bienvenido, " +
                                                    _usuario.text +
                                                    "!",
                                                Colors.green);
                                            Navigator.of(context).pop();
                                            context
                                                .read<OhanamiBloc>()
                                                .add(MostrarListas(us));
                                          });

                                          return;
                                        }
                                        showDialog(
                                          context: context,
                                          builder: (context) => Center(
                                            child: SingleChildScrollView(
                                              physics:
                                                  const BouncingScrollPhysics(),
                                              child: AlertDialog(
                                                title:
                                                    const Text("Error de red"),
                                                content: const Text(
                                                    "No se pudo establecer la conexión con la base de datos."),
                                                actions: [
                                                  Column(children: [
                                                    Row(
                                                      mainAxisAlignment:
                                                          MainAxisAlignment
                                                              .center,
                                                      children: [
                                                        ElevatedButton(
                                                            onPressed: () {
                                                              Navigator.of(
                                                                      context)
                                                                  .pop();
                                                            },
                                                            child: const Text(
                                                                "OK")),
                                                      ],
                                                    ),
                                                  ]),
                                                ],
                                              ),
                                            ),
                                          ),
                                        );
                                      }
                                    },
                                    child: const Text("REGISTRARSE")),
                              ],
                            ),
                          ]),
                        ),
                      ],
                    ),
                  ),
                );
              });
            }
            return Container();
          }),
      FutureBuilder(
          future: FlutterSession().get("tipoConexion"),
          builder: (context, snapshot) {
            if (snapshot.data == 0) {
              return _elementoLista("Eliminar datos locales", Icons.logout, () {
                Navigator.pop(context);
                showDialog(
                  context: context,
                  builder: (context) => AlertDialog(
                    title: const Text("⚠️ Advertencia"),
                    content: const Text(
                        "Está por eliminar sus datos, ¿desea continuar?"),
                    actions: [
                      TextButton(
                          onPressed: () async {
                            _destruir();
                          },
                          child: const Text("Sí")),
                      ElevatedButton(
                          onPressed: () {
                            Navigator.pop(context);
                          },
                          child: const Text("No")),
                    ],
                  ),
                  barrierDismissible: false,
                );
              });
            }
            return _elementoLista("Cerrar sesión", Icons.logout, () {
              showAlertDialog(context);
              _destruir();
            });
          }),
      FutureBuilder(
          future: FlutterSession().get("tipoConexion"),
          builder: (context, snapshot) {
            if (snapshot.data == 0) {
              return Center(
                child: Container(
                    margin: const EdgeInsets.fromLTRB(30, 10, 30, 0),
                    child: const Text(
                      "<< Esta acción es irreversible >>",
                      style: TextStyle(color: Colors.red, fontSize: 17),
                    )),
              );
            }
            return Container();
          }),
    ]));
  }

  _destruir() {
    Navigator.pop(context);
    showAlertDialog(context);
    context.read<OhanamiBloc>().add(Desconectarse());

    Future.delayed(const Duration(seconds: 3), () {
      Navigator.pop(context);
      context.read<OhanamiBloc>().add(VerificarUsuario());
    });

    btnConectarse = false;
  }

  _floatingActionButton() {
    return SizedBox(
      width: 250,
      child: FittedBox(
        child: FloatingActionButton.extended(
            label:
                const Text("Agregar partida", style: TextStyle(fontSize: 20)),
            icon: const Icon(Icons.edit),
            backgroundColor: tema,
            onPressed: () async {
              context.read<OhanamiBloc>().add(AgregarNuevaPartida());
            }),
      ),
    );
  }

  _elementoLista(text, icon, function) {
    return ListTile(
      leading: Icon(icon),
      title: Text(text),
      onTap: function,
    );
  }
}
