
import 'package:flutter/material.dart';

class VistaIniciando extends StatefulWidget {
  const VistaIniciando({ Key? key }) : super(key: key);

  @override
  _VistaIniciandoState createState() => _VistaIniciandoState();
}

class _VistaIniciandoState extends State<VistaIniciando> {
  @override
  Widget build(BuildContext context) {
    return Center(
      child: Column(
        mainAxisAlignment: MainAxisAlignment.center,
        children: [
          const Text("Iniciando..."),
          Container(
            margin: const EdgeInsets.fromLTRB(100, 20, 100, 20),
            child: const LinearProgressIndicator(
              minHeight: 6,
            ),
            ),
        ],
      ),
    );
  }
}