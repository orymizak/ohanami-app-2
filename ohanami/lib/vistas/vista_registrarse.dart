import 'package:flutter/services.dart';
import 'package:ohanami/vistas/exports/exports.dart';

class VistaRegistro extends StatefulWidget {
  const VistaRegistro({Key? key}) : super(key: key);

  @override
  _VistaRegistroState createState() => _VistaRegistroState();
}

class _VistaRegistroState extends State<VistaRegistro> {
  final _control = GlobalKey<FormState>();
  final TextEditingController _usuario = TextEditingController(),
      _telefono = TextEditingController(),
      _contra = TextEditingController(),
      _contra2 = TextEditingController();

  @override
  void initState() {
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        leading: IconButton(
            icon: const Icon(Icons.arrow_back, color: Colors.white),
            onPressed: () =>
                context.read<OhanamiBloc>().add(VerificarUsuario())),
        centerTitle: true,
        title: const Text("Registrarse"),
        backgroundColor: tema,
      ),
      body: SingleChildScrollView(
        child: Form(
          key: _control,
          child: Padding(
            padding: const EdgeInsets.all(15.0),
            child: Column(
              children: [
                const Text("Formulario de registro.", style: titulos6),
                espacioCH,
                Text("Ingresa los siguientes campos para continuar:",
                    style: titulos5z),
                espacio,
                Padding(
                  padding: const EdgeInsets.all(8.0),
                  child: TextFormField(
                      autofocus: true,
                      controller: _usuario,
                      textInputAction: TextInputAction.next,
                      inputFormatters: <TextInputFormatter>[
                        FilteringTextInputFormatter.allow(RegExp(r'[A-Za-z]')),
                      ],
                      keyboardType: TextInputType.name,
                      maxLength: 8,
                      buildCounter: (
                        BuildContext context, {
                        required int currentLength,
                        int? maxLength,
                        required bool isFocused,
                      }) =>
                          null,
                      validator: (value) {
                        if (value == null ||
                            value.length < 3 ||
                            value.length > 8) {
                          return 'Ingresa de 3 a 8 caracteres';
                        }
                        return null;
                      },
                      decoration: const InputDecoration(
                          labelText: "Nombre de usuario",
                          border: OutlineInputBorder(),
                          hintText: "3 - 8 caracteres")),
                ),
                Padding(
                  padding: const EdgeInsets.all(8.0),
                  child: TextFormField(
                      autofocus: true,
                      controller: _contra,
                      obscureText: true,
                      textInputAction: TextInputAction.next,
                      buildCounter: (
                        BuildContext context, {
                        required int currentLength,
                        int? maxLength,
                        required bool isFocused,
                      }) =>
                          null,
                      validator: (value) {
                        if (value == null || value.length < 7) {
                          return 'Ingresa al menos 7 caracteres';
                        }
                        return null;
                      },
                      decoration: const InputDecoration(
                          labelText: "Contraseña",
                          border: OutlineInputBorder(),
                          hintText: "Al menos 8 caracteres")),
                ),
                Padding(
                  padding: const EdgeInsets.all(8.0),
                  child: TextFormField(
                      autofocus: true,
                      controller: _contra2,
                      obscureText: true,
                      textInputAction: TextInputAction.next,
                      buildCounter: (
                        BuildContext context, {
                        required int currentLength,
                        int? maxLength,
                        required bool isFocused,
                      }) =>
                          null,
                      validator: (value) {
                        if (value == null || value.length < 7) {
                          return 'Ingresa tu contraseña correctamente';
                        }
                        if (value != _contra.text) {
                          return 'Las contraseñas no coinciden';
                        }
                        return null;
                      },
                      decoration: const InputDecoration(
                          labelText: "Repite tu contraseña",
                          border: OutlineInputBorder(),
                          hintText: "Repite tu contraseña")),
                ),
                Padding(
                  padding: const EdgeInsets.all(8.0),
                  child: TextFormField(
                      autofocus: true,
                      controller: _telefono,
                      maxLength: 10,
                      textInputAction: TextInputAction.go,
                      inputFormatters: <TextInputFormatter>[
                        FilteringTextInputFormatter.allow(RegExp(r'[0-9]')),
                      ],
                      keyboardType: TextInputType.number,
                      buildCounter: (
                        BuildContext context, {
                        required int currentLength,
                        int? maxLength,
                        required bool isFocused,
                      }) =>
                          null,
                      validator: (value) {
                        if (value == null || value.length != 10) {
                          return 'Verifica que el número sea de 10 dígitos';
                        }
                        return null;
                      },
                      decoration: const InputDecoration(
                          labelText: "Ingresa tu teléfono",
                          border: OutlineInputBorder(),
                          hintText: "Ingresa 10 dígitos")),
                ),
                ElevatedButton(
                    onPressed: () async {
                      if (_control.currentState!.validate()) {
                        Usuario us = Usuario(
                            usuario: _usuario.text,
                            telefono: int.parse(_telefono.text),
                            password: _contra.text,
                            partidas: []);

                        if (await usuarioRemoto.checarRed() == true) {
                          await usuarioRemoto.registradoUsuario(usuarioRemoto: us) == true
                              ? widget.toast(
                                  context,
                                  "El usuario " +
                                      _usuario.text +
                                      " ya se encuentra registrado.",
                                  Colors.red)
                              : await usuarioRemoto
                                  .registrarUsuario(usuarioRemoto: us)
                                  .then((value) {
                                  widget.toast(
                                      context,
                                      "¡Registro exitoso! Bienvenido, " +
                                          _usuario.text +
                                          "!",
                                      Colors.green);
                                      context.read<OhanamiBloc>().add(MostrarListas(us));
                                });
                          return;
                        }
                        widget.toast(
                            context,
                            "Error: no se pudo establecer conexión con la base de datos.",
                            Colors.red);
                      }
                    },
                    child: const Text("REGISTRARSE")),
              ],
            ),
          ),
        ),
      ),
    );
  }
}
