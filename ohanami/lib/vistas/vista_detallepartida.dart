import 'package:flutter_sesion/flutter_session.dart';
import 'exports/export_detallep.dart';
import 'package:intl/intl.dart';

class DetallePartida extends StatefulWidget {
  const DetallePartida({Key? key, required this.partida, required this.usuario})
      : super(key: key);
  final Partida partida;
  final Usuario usuario;

  @override
  _DetallePartidaState createState() => _DetallePartidaState(partida, usuario);
}

class _DetallePartidaState extends State<DetallePartida> {
  Partida partida;
  Usuario us;
  List<PuntuacionJugador> prDesenlace = [];
  List<PuntuacionJugador> puntuacionesronda1 = [];
  List<PuntuacionJugador> puntuacionesronda2 = [];
  List<PuntuacionJugador> puntuacionesronda3 = [];
  List<BarChartGroupData> list = [], listInd = [];
  List<Column> listaJ = [];
  List listaG = [];
  String nombreganador = '';

  _DetallePartidaState(this.partida, this.us);

  @override
  void initState() {
    sacarDatos();
    ganador();
    nombre();
    datosJugadores(partida);
    super.initState();
  }

  void ganador() {
    for (int i = 0; partida.jugadores.length > i; i++) {
      listaG.add(prDesenlace[i].total);
    }
    listaG.sort((a, b) => b.compareTo(a));
    // print(listaG[0]);
  }

  void nombre() {
    int count = 0;
    for (int i = 0; partida.jugadores.length > i; i++) {
      if (prDesenlace[i].total == listaG[0]) {
        if (count > 0) nombreganador = nombreganador + ", ";
        nombreganador = nombreganador + partida.jugadores.elementAt(i).nombre;
        count++;
      }
    }
    if (count > 1) nombreganador = nombreganador + " (empate)";
    if (count > 3) nombreganador = "victoria grupal";
    if (listaG[0] == 0) nombreganador = "victoria secreta";
    // print(nombreganador);
  }

  void datosJugadores(partida) {
    for (int i = 0; partida.jugadores.length > i; i++) {
      listaJ.add(Column(
        children: [
          Row(
            mainAxisAlignment: MainAxisAlignment.spaceEvenly,
            children: [
              Row(
                crossAxisAlignment: CrossAxisAlignment.center,
                children: [
                  Container(
                    width: 95,
                    child: Row(
                      children: [
                        SizedBox(
                            child: listaG[0] == prDesenlace.elementAt(i).total
                                ? Text(
                                    partida.jugadores.elementAt(i).nombre +
                                        "  ",
                                    style:
                                        TextStyle(fontWeight: FontWeight.bold),
                                  )
                                : Text(partida.jugadores.elementAt(i).nombre)),
                        SizedBox(
                            width: 5,
                            child: listaG[0] == prDesenlace.elementAt(i).total
                                ? Icon(Icons.emoji_events, color: Colors.yellow)
                                : Container()),
                      ],
                    ),
                  ),
                  espacio,
                ],
              ),
              Row(
                children: [
                  SizedBox(
                      width: 40,
                      child: Text(prDesenlace.elementAt(i).porAzules.toString(),
                          textAlign: TextAlign.center, style: titulos5x)),
                  SizedBox(
                      width: 40,
                      child: Text(prDesenlace.elementAt(i).porVerdes.toString(),
                          textAlign: TextAlign.center, style: titulos5x)),
                  SizedBox(
                      width: 40,
                      child: Text(prDesenlace.elementAt(i).porRosas.toString(),
                          textAlign: TextAlign.center, style: titulos5x)),
                  SizedBox(
                      width: 40,
                      child: Text(prDesenlace.elementAt(i).porNegras.toString(),
                          textAlign: TextAlign.center, style: titulos5x)),
                  espacio,
                  SizedBox(
                      width: 40,
                      child: Text(prDesenlace.elementAt(i).total.toString(),
                          textAlign: TextAlign.center, style: titulos5y)),
                ],
              )
            ],
          ),
          espacioCH,
        ],
      ));
    }
  }

  void sacarDatos() {
    sacarPuntuaciones();

    for (int i = 0; partida.jugadores.length > i; i++) {
      double pr1Az = puntuacionesronda1.elementAt(i).porAzules.toDouble();

      double pr2Az = puntuacionesronda2.elementAt(i).porAzules.toDouble();
      double pr2Ve = puntuacionesronda2.elementAt(i).porVerdes.toDouble();

      double pr3Az = puntuacionesronda3.elementAt(i).porAzules.toDouble();
      double pr3Ve = puntuacionesronda3.elementAt(i).porVerdes.toDouble();
      double pr3Ro = puntuacionesronda3.elementAt(i).porRosas.toDouble();
      double pr3Ne = puntuacionesronda3.elementAt(i).porNegras.toDouble();

      double pttAz = prDesenlace.elementAt(i).porAzules.toDouble();
      double pttVe = prDesenlace.elementAt(i).porVerdes.toDouble();
      double pttRo = prDesenlace.elementAt(i).porRosas.toDouble();
      double pttNe = prDesenlace.elementAt(i).porNegras.toDouble();

      var at = pr1Az + pr2Az + pr3Az;
      var rt = pr1Az + pr2Az + pr3Az + pr2Ve + pr3Ve + pr3Ro;
      var nt = pr1Az + pr2Az + pr3Az + pr2Ve + pr3Ve + pr3Ro + pr3Ne;

      list.add(BarChartGroupData(x: i, barsSpace: 4, barRods: [
        BarChartRodData(
            rodStackItems: [
              BarChartRodStackItem(0, pr1Az + 0.00001, Colors.blue.shade700),
            ],
            colors: [
              Colors.red
            ],
            width: ancho * 0.035,
            y: pr1Az + 0.00001,
            borderRadius: const BorderRadius.vertical(top: Radius.circular(4))),
        BarChartRodData(
            rodStackItems: [
              BarChartRodStackItem(0, pr1Az + pr2Az, Colors.blue.shade700),
              BarChartRodStackItem(pr1Az + pr2Az,
                  pr1Az + pr2Az + pr2Ve + 0.00001, Colors.green.shade700),
            ],
            colors: [
              Colors.red
            ],
            width: ancho * 0.035,
            y: pr1Az + pr2Az + pr2Ve + 0.00001,
            borderRadius: const BorderRadius.vertical(top: Radius.circular(4))),
        BarChartRodData(
            rodStackItems: [
              BarChartRodStackItem(0, at, Colors.blue.shade700),
              BarChartRodStackItem(
                  at, at + pr2Ve + pr3Ve, Colors.green.shade700),
              BarChartRodStackItem(
                  at + pr2Ve + pr3Ve, rt, Colors.pink.shade300),
              BarChartRodStackItem(rt, nt + 0.00001, Colors.black),
            ],
            colors: [
              Colors.red
            ],
            width: ancho * 0.035,
            y: nt + 0.00001,
            borderRadius: const BorderRadius.vertical(top: Radius.circular(4))),
      ]));

      listInd.add(BarChartGroupData(x: i, barsSpace: 4, barRods: [
        BarChartRodData(
            y: pttAz + 0.00001,
            rodStackItems: [
              BarChartRodStackItem(0, pr1Az, Colors.blue.shade900),
              BarChartRodStackItem(pr1Az, pr1Az + pr2Az, Colors.blue.shade600),
              BarChartRodStackItem(
                  pr1Az + pr2Az, pttAz + 0.00001, Colors.blue.shade300),
            ],
            width: ancho * 0.035,
            colors: [Colors.blue.shade300],
            borderRadius: const BorderRadius.vertical(top: Radius.circular(4))),
        BarChartRodData(
            y: pttVe + 0.00001,
            rodStackItems: [
              BarChartRodStackItem(0, pr2Ve, Colors.green.shade800),
              BarChartRodStackItem(pr2Ve, pttVe + 0.00001, Colors.green),
            ],
            width: ancho * 0.035,
            colors: [Colors.green],
            borderRadius: const BorderRadius.vertical(top: Radius.circular(4))),
        BarChartRodData(
            y: pttRo + 0.00001,
            rodStackItems: [
              BarChartRodStackItem(0, pttRo + 0.00001, Colors.pink),
            ],
            width: ancho * 0.035,
            colors: [Colors.pink],
            borderRadius: const BorderRadius.vertical(top: Radius.circular(4))),
        BarChartRodData(
            y: pttNe + 0.00001,
            rodStackItems: [
              BarChartRodStackItem(0, pttNe + 0.00001, Colors.black),
            ],
            width: ancho * 0.035,
            colors: [Colors.black],
            borderRadius: const BorderRadius.vertical(top: Radius.circular(4))),
      ]));
    }
  }

  void sacarPuntuaciones() {
    puntuacionesronda1 = partida.puntos(ronda: FasePuntuacion.ronda1);
    puntuacionesronda2 = partida.puntos(ronda: FasePuntuacion.ronda2);
    puntuacionesronda3 = partida.puntos(ronda: FasePuntuacion.ronda3);
    prDesenlace = partida.puntos(ronda: FasePuntuacion.desenlace);
    prDesenlace = partida.puntos(ronda: FasePuntuacion.desenlace);
  }

  _grafica() {
    return BarChart(
      BarChartData(
        barTouchData: BarTouchData(
          touchTooltipData: BarTouchTooltipData(
              tooltipBgColor: Colors.white,
              tooltipMargin: -45.0,
              fitInsideVertically: true,
              getTooltipItem: (group, groupIndex, rod, rodIndex) {
                String result;
                switch (rodIndex.toInt()) {
                  case 0:
                    result = 'Puntuación ronda 1:';
                    break;
                  case 1:
                    result = 'Puntuación ronda 2:';
                    break;
                  case 2:
                    result = 'Puntuación ronda 3:';
                    break;
                  default:
                    throw Error();
                }
                return BarTooltipItem(
                  result + '\n',
                  const TextStyle(
                    color: Colors.black,
                    fontWeight: FontWeight.bold,
                    fontSize: 18,
                  ),
                  children: <TextSpan>[
                    TextSpan(
                      text: (rod.y).toInt().toString(),
                      style: const TextStyle(
                        color: Colors.red,
                        fontSize: 18,
                        fontWeight: FontWeight.w500,
                      ),
                    ),
                  ],
                );
              }),
        ),
        alignment: BarChartAlignment.center,
        titlesData: FlTitlesData(
          show: true,
          bottomTitles: SideTitles(
            showTitles: true,
            getTextStyles: (context, value) =>
                const TextStyle(color: Colors.black, fontSize: 15),
            margin: 10,
            getTitles: (double value) {
              for (int i = value.toInt(); partida.jugadores.length > i; i++) {
                return partida.jugadores.elementAt(i).nombre.toString();
              }
              return '';
            },
          ),
          leftTitles: SideTitles(
            showTitles: true,
            reservedSize: 28,
            getTextStyles: (context, value) =>
                const TextStyle(color: Colors.black, fontSize: 15),
            margin: 5,
          ),
          topTitles: SideTitles(showTitles: false),
          rightTitles: SideTitles(showTitles: false),
        ),
        gridData: FlGridData(
          show: true,
          checkToShowHorizontalLine: (value) => value % 10 == 0,
          getDrawingHorizontalLine: (value) => FlLine(
            color: Colors.black,
            strokeWidth: 0.25,
          ),
          verticalInterval: 0.08,
        ),
        borderData: FlBorderData(
          show: true,
        ),
        groupsSpace: 40,
        // backgroundColor: Colors.white,
        backgroundColor: Colors.blue.shade50,
        barGroups: list,
      ),
    );
  }

  _graficaInd() {
    return BarChart(
      BarChartData(
        barTouchData: BarTouchData(
          touchTooltipData: BarTouchTooltipData(
              tooltipBgColor: Colors.white,
              tooltipMargin: -45.0,
              fitInsideVertically: true,
              getTooltipItem: (group, groupIndex, rod, rodIndex) {
                String result;
                switch (rodIndex.toInt()) {
                  case 0:
                    result = 'Puntos cartas azules:';
                    break;
                  case 1:
                    result = 'Puntos cartas verdes:';
                    break;
                  case 2:
                    result = 'Puntos cartas rosas:';
                    break;
                  case 3:
                    result = 'Puntos cartas negras:';
                    break;
                  default:
                    throw Error();
                }
                return BarTooltipItem(
                  result + '\n',
                  const TextStyle(
                    color: Colors.black,
                    fontWeight: FontWeight.bold,
                    fontSize: 18,
                  ),
                  children: <TextSpan>[
                    TextSpan(
                      text: (rod.y).toInt().toString(),
                      style: const TextStyle(
                        color: Colors.red,
                        fontSize: 18,
                        fontWeight: FontWeight.w500,
                      ),
                    ),
                  ],
                );
              }),
        ),
        alignment: BarChartAlignment.center,
        titlesData: FlTitlesData(
          show: true,
          bottomTitles: SideTitles(
            showTitles: true,
            getTextStyles: (context, value) =>
                const TextStyle(color: Colors.black, fontSize: 15),
            margin: 10,
            getTitles: (double value) {
              for (int i = value.toInt(); partida.jugadores.length > i; i++) {
                return partida.jugadores.elementAt(i).nombre.toString();
              }
              return '';
            },
          ),
          leftTitles: SideTitles(
            showTitles: true,
            reservedSize: 28,
            getTextStyles: (context, value) =>
                const TextStyle(color: Colors.black, fontSize: 15),
            margin: 5,
          ),
          topTitles: SideTitles(showTitles: false),
          rightTitles: SideTitles(showTitles: false),
        ),
        gridData: FlGridData(
          show: true,
          checkToShowHorizontalLine: (value) => value % 10 == 0,
          getDrawingHorizontalLine: (value) => FlLine(
            color: Colors.black,
            strokeWidth: 0.25,
          ),
          verticalInterval: 0.08,
        ),
        borderData: FlBorderData(
          show: true,
        ),
        groupsSpace: 23,
        // backgroundColor: Colors.white,
        backgroundColor: Colors.blue.shade50,
        barGroups: listInd,
      ),
    );
  }

  @override
  Widget build(BuildContext context) {
    DateFormat dateFormat = DateFormat("dd/MM/yyyy - HH:mm");
    String string = dateFormat.format(partida.fecha);
    var dat = partida.fecha.hashCode;

    return DefaultTabController(
      length: 2,
      child: Scaffold(
        appBar: AppBar(
            backgroundColor: tema,
            centerTitle: true,
            title: const Text("Detalles de la partida"),
            leading: FutureBuilder(
                future: FlutterSession().get("usuario"),
                builder: (context, snapshot) {
                  return IconButton(
                      onPressed: () async => context.read<OhanamiBloc>().add(
                          MostrarListas(Usuario.fromJson(
                              await FlutterSession().get("datos")))),
                      icon: const Icon(Icons.arrow_back));
                })),
        body: SingleChildScrollView(
          child: Padding(
            padding: const EdgeInsets.all(8.0),
            child: Container(
              decoration: BoxDecoration(
                color: Colors.blue.shade200,
                borderRadius: const BorderRadius.all(Radius.circular(5)),
              ),
              width: ancho,
              child: Column(
                children: [
                  espacioCH,
                  Text("Partida #" + dat.toString(), style: titulos7),
                  espacioCH,
                  Text(string + " h."),
                  const Divider(),
                  Text("Victoria:", style: titulos5y),
                  Text(nombreganador, style: titulos5x),
                  const Divider(),
                  espacioCH,
                  Row(
                    mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                    children: [
                      SizedBox(
                          width: 85,
                          child: Text("JUGADORES", style: titulos5x)),
                      espacio,
                      _cartas(),
                    ],
                  ),
                  espacioCH,
                  Column(
                    children: listaJ,
                  ),
                  const Divider(),
                  Padding(
                    padding: const EdgeInsets.all(10.0),
                    child: Container(
                      decoration: BoxDecoration(
                        borderRadius: BorderRadius.circular(8),
                        color: tema,
                      ),
                      height: 48,
                      child: TabBar(
                        indicator: BoxDecoration(
                          borderRadius: BorderRadius.circular(8),
                          color: Colors.blueGrey.shade900,
                        ),
                        tabs: const [
                          Tab(text: "Puntuación acumulada"),
                          Tab(text: "Puntuación individual"),
                        ],
                      ),
                    ),
                  ),
                  SizedBox(
                    height: 320,
                    child: TabBarView(
                      physics: const NeverScrollableScrollPhysics(),
                      children: [
                        Container(
                          padding: const EdgeInsets.fromLTRB(0, 10, 10, 0),
                          decoration: BoxDecoration(
                            borderRadius: BorderRadius.circular(5),
                            // color: Colors.red,
                          ),
                          child: _grafica(),
                        ),
                        Container(
                          padding: const EdgeInsets.fromLTRB(0, 10, 10, 0),
                          decoration: BoxDecoration(
                            borderRadius: BorderRadius.circular(5),
                            // color: Colors.red,
                          ),
                          child: _graficaInd(),
                        ),
                      ],
                    ),
                  ),
                  espacioCH,
                ],
              ),
            ),
          ),
        ),
      ),
    );
  }
}

_imagenCarta(src) {
  return SizedBox(width: 40, child: Image.asset(src));
}

_cartas() {
  return Row(
    mainAxisAlignment: MainAxisAlignment.center,
    children: [
      _imagenCarta("lib/src/cartaazul.png"),
      _imagenCarta("lib/src/cartaverde.png"),
      _imagenCarta("lib/src/cartarosa.png"),
      _imagenCarta("lib/src/cartanegra.png"),
      espacio,
      _imagenCarta("lib/src/cartas.png"),
    ],
  );
}
